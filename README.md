## Analizador léxico.

Proyecto realizado para el curso de LP03 de la Universidad de la Salle. Las tecnologías utilizadas para el desarrollo de este proyecto fueron:

- HTML, CSS y Bootstrap. (Para el maquetado)
- Codemirror
- Javascript JS.
- JQuery.
- Angular 9.

## Uso

Para utilizar la webapp solo es necesario ejecutar el archivo index.html de la carpeta public (analizador_lex\public).
